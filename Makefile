# Makefile for doctorj
# Borrowed/copied from ESR.

PACKAGE=doctorj
VERSION=5.0.0

prefix=/usr
mandir=$(prefix)/share/man
datadir=$(prefix)/share
bindir=$(prefix)/bin

DOCS      = doc/$(PACKAGE).1 doc/$(PACKAGE).xml etc/words.en_CA etc/words.en_UK etc/words.en_US
SOURCES   = bin/doctorj Makefile lib/doctorj.jar $(DOCS) $(PACKAGE).spec
# DISTFILES = org

all:
	mkdir classes
	find org -name '*.java' | xargs javac -d classes
	mkdir lib
	cd classes && jar cf ../lib/doctorj.jar .

install: doc/$(PACKAGE).1
	mkdir -p $(DESTDIR)$(bindir)
	mkdir -p $(DESTDIR)$(datadir)/doctorj
	mkdir -p $(DESTDIR)$(mandir)/man1
	cp -p bin/$(PACKAGE) $(DESTDIR)$(bindir)
	@# gzipping the man page conflicts with Mandrake, which bzips it:
	@# gzip \<doc/$(PACKAGE).1 \>$(DESTDIR)$(mandir)/man1/$(PACKAGE).1.gz
	cp -p etc/words.*         $(DESTDIR)$(datadir)/doctorj
	cp -p lib/$(PACKAGE).jar  $(DESTDIR)$(datadir)/doctorj
	cp -p doc/$(PACKAGE).1    $(DESTDIR)$(mandir)/man1/$(PACKAGE).1

uninstall:
	@echo % rm -f   $(DESTDIR)$(bindir)/$(PACKAGE)
	@echo % -rm -f  $(DESTDIR)$(mandir)/man1/$(PACKAGE).1*
	@echo % -rm -rf $(DESTDIR)$(datadir)/doctorj
	@echo % -rm -f  $(DESTDIR)$(prefix)/man/man1/doctorj.1*

$(PACKAGE)-$(VERSION).tar.gz: $(SOURCES)
	@mkdir $(PACKAGE)-$(VERSION)
	@echo copying $(SOURCES) 
	@cp -r $(SOURCES) $(PACKAGE)-$(VERSION)
	@tar -cvzf $(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION)
	@#rm -fr $(PACKAGE)-$(VERSION)

dist: $(PACKAGE)-$(VERSION).tar.gz

# The following rules are not for public use.

# Builds the HTML version of the man page.
doc/www/$(PACKAGE).html: doc/$(PACKAGE).pod
	pod2html --noindex --verbose --css=$(PACKAGE)man.css $^ > $@
