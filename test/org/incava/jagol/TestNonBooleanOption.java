package org.incava.jagol;

import java.io.*;
import java.util.*;
import junit.framework.TestCase;
import org.incava.log.Log;


public class TestNonBooleanOption extends TestCase
{
    NonBooleanOption opt = new NonBooleanOption("nbopt", "this is the description of nbopt") {
            public void setValue(String value) throws InvalidTypeException
            {        
            }

            protected String getType()
            {
                return "";
            }
        };
    
    public TestNonBooleanOption(String name)
    {
        super(name);
    }

    public void testSetFromArgsListEqual()
    {
        List args = new ArrayList();
        try {
            boolean processed = opt.set("--nbopt=444", args);
            assertEquals("option processed", true, processed);
            assertEquals("argument removed from list", 0, args.size());
        }
        catch (OptionException ite) {
            fail("failure is not an option");
        }
    }

    public void testSetFromArgsListSeparateString()
    {
        List args = new ArrayList();
        args.add("41");
        try {
            boolean processed = opt.set("--nbopt", args);
            assertEquals("option processed", true, processed);
            assertEquals("argument removed from list", 0, args.size());
        }
        catch (OptionException ite) {
            fail("failure is not an option");
        }
    }

    public void testSetFromLongerArgsListEqual()
    {
        List args = new ArrayList();
        args.add("--anotheropt");
        try {
            boolean processed = opt.set("--nbopt=666", args);
            assertEquals("option processed", true, processed);
            assertEquals("argument removed from list", 1, args.size());
        }
        catch (OptionException ite) {
            fail("failure is not an option");
        }
    }

    public void testSetFromLongerArgsListSeparateString()
    {
        List args = new ArrayList();
        args.add("1234");
        args.add("--anotheropt");
        try {
            boolean processed = opt.set("--nbopt", args);
            assertEquals("option processed", true, processed);
            assertEquals("argument removed from list", 1, args.size());
        }
        catch (OptionException ite) {
            fail("failure is not an option");
        }
    }

    public void testSetInvalidValueDanglingEquals()
    {
        List args = new ArrayList();
        args.add("--anotheropt");
        try {
            boolean processed = opt.set("--nbopt=", args);
            fail("exception expected");
        }
        catch (OptionException ite) {
        }
    }

}

