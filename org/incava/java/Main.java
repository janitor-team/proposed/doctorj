package org.incava.java;

import java.io.*;
import java.util.*;


public class Main
{
    public static void main(String[] args)
    {
        long totalInitTime  = 0;
        long totalParseTime = 0;
        long totalDumpTime  = 0;

        Java13Parser parser = null;
        int i = 0;
        boolean astDump = false;
        
        if (args.length > 0 && args[0].equals("--dump")) {
            astDump = true;
            ++i;
        }

        for (; i < args.length; ++i) {
            String filename = args[i];
            long   initTime;

            // System.out.println("Reading from file " + filename + " . . .");
            try {
                long initStartTime = System.currentTimeMillis();
                if (parser == null) {
                    parser = new Java13Parser(new FileInputStream(filename));
                }
                else {
                    parser.ReInit(new FileInputStream(filename));
                }
                long initStopTime = System.currentTimeMillis();
                initTime = initStopTime - initStartTime;
                totalInitTime  += initTime;
            }
            catch (FileNotFoundException e) {
                System.out.println("File " + filename + " not found.");
                continue;
            }

            try {
                long parseStartTime = System.currentTimeMillis();
                ASTCompilationUnit cu = parser.CompilationUnit();
                
                long parseStopTime  = System.currentTimeMillis();
                long parseTime      = parseStopTime - parseStartTime;

                totalParseTime += parseTime;

                long dumpTime = 0;

                if (astDump) {
                    long dumpStartTime = System.currentTimeMillis();

                    cu.dump();

                    long dumpStopTime = System.currentTimeMillis();
                    dumpTime = dumpStopTime - dumpStartTime;
                    totalDumpTime += dumpTime;
                }

                System.out.println("    " + filename);
                System.out.println("        init time : " + initTime  + " ms.");
                System.out.println("        parse time: " + parseTime + " ms.");
                if (astDump) {
                    System.out.println("        dump time : " + dumpTime  + " ms.");
                }
                System.out.println("        total time: " + (initTime + parseTime) + " ms.");
            }
            catch (ParseException e) {
                System.out.println(e.getMessage());
                System.out.println("Encountered errors during parse.");
            }
        }

        System.out.println("    " + args.length + " files");
        System.out.println("        init time : " + totalInitTime  + " ms.");
        System.out.println("        parse time: " + totalParseTime + " ms.");
        if (astDump) {
            System.out.println("        dump time : " + totalDumpTime  + " ms.");
        }
        System.out.println("        total time: " + (totalInitTime + totalParseTime + totalDumpTime) + " ms.");
    }

}
