package org.incava.analysis;

import org.incava.java.*;
import org.incava.log.Log;


/**
 * Runs visit on all children of each node for which this is called.
 */
public class Rule extends DefaultJavaParserVisitor
{
    /**
     * The report to which this rule will send violations.
     */
    protected Report report;

    /**
     * Creates a rule with a report.
     *
     * @param r The report that this rule sends violations to.
     */
    public Rule(Report r)
    {
        report = r;
    }

    /**
     * Adds a violation to the report.
     * 
     * @param v The violation, which is sends to the report.
     */
    protected void addViolation(Violation v)
    {
        Log.log("adding violation: " + v);
        report.addViolation(v);
    }

}

