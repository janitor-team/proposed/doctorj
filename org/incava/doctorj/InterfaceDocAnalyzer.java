package org.incava.doctorj;

import java.util.List;
import org.incava.analysis.Report;
import org.incava.java.*;
import org.incava.log.Log;


/**
 * Analyzes Javadoc and code for an interface.
 */
public class InterfaceDocAnalyzer extends TypeDocAnalyzer
{
    public InterfaceDocAnalyzer(Report r, SimpleNode node)
    {
        super(r, node);
    }

    public String getItemType() 
    {
        return "interface";
    }

    /**
     * Returns the valid tags, as strings, for interfaces.
     */
    protected List getValidTags()
    {
        return JavadocTags.getValidInterfaceTags();
    }

}
