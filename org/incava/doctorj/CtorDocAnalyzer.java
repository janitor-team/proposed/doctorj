package org.incava.doctorj;

import java.util.Iterator;
import java.util.List;
import org.incava.analysis.Report;
import org.incava.java.*;
import org.incava.log.Log;


/**
 * Analyzes Javadoc and code for constructors.
 */
public class CtorDocAnalyzer extends FunctionDocAnalyzer
{
    private ASTConstructorDeclaration ctor;
    
    public CtorDocAnalyzer(Report r, ASTConstructorDeclaration ctor)
    {
        super(r, ctor);
        this.ctor = ctor;
    }

    public String getItemType() 
    {
        return "constructor";
    }

    /**
     * Returns the throws list for the constructor.
     */
    protected ASTFormalParameters getParameterList()
    {
        return (ASTFormalParameters)ctor.jjtGetChild(0);
    }

    /**
     * Returns the valid tags, as strings, for ctors.
     */
    protected List getValidTags()
    {
        return JavadocTags.getValidConstructorTags();
    }

    /**
     * Adds a violation for a constructor, with the violation pointing to the
     * constructor name.
     */
    protected void addUndocumentedViolation(String desc)
    {
        List children = ctor.getChildren();
        Iterator it = children.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Token) {
                Token t = (Token)obj;
                // everything else is a modifier:
                if (t.kind == Java14ParserConstants.STRING_LITERAL) {
                    addViolation(desc, t);
                    break;
                }
            }
        }
    }

}
